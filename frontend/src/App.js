import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";

import {Container} from "reactstrap";
import Login from "./containers/Login/Login";
import ProductLink from "./containers/ProductLink/ProductLink";
import Register from "./containers/Register/Register";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/usersActions";
import {NotificationContainer} from "react-notifications";
import OneItem from "./containers/OneItem/OneItem";
import AddNewItem from "./containers/AddNewItem/AddNewItem";
import OneProduct from "./containers/OneProduct/OneProduct";

class App extends Component{
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                </header>
                    <Container>
                    <Switch>
                        <Route path="/" exact component={ProductLink}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/add_item" exact component={AddNewItem}/>
                        <Route path="/product/:id" exact component={OneItem}/>
                        <Route path="/category/:id" component={OneProduct}/>

                    </Switch>
                    </Container>

            </Fragment>

      );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});


export default connect(mapStateToProps, mapDispatchToProps)(App);

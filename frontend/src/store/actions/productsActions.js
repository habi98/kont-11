import axios from '../../axios-api'
import {push} from "connected-react-router";

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const  FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_ONE_ITEM_SUCCESS = 'FETCH_ONE_ITEM_SUCCESS';

export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});

export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, product});

export const fetchOneProductSuccess = product => ({type: FETCH_ONE_ITEM_SUCCESS, product});
export const createProductSuccess =()  => ({type: CREATE_PRODUCT_SUCCESS});

export const fetchProducts = () => {
    return dispatch => {
        axios.get('/products').then(
            response => dispatch(fetchProductsSuccess(response.data))
        )
    }
};

export const fetchProduct = id => {
    return dispatch => {
        axios.get('/products?category=' + id).then(
            response => dispatch(fetchProductSuccess(response.data))
        )
    }
};

export const fetchOneProduct = (id) => {
    return dispatch => {
        axios.get('/products/' + id).then(
            response =>  dispatch(fetchOneProductSuccess(response.data))
        )
    }
};

export const createProduct = (productData) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if (!user) {
            dispatch(push('/login'));
        } else {
            axios.post('/products', productData, {headers: {'Authorization': user.token}}).then(
                () => {
                    dispatch(createProductSuccess());
                    dispatch(push('/'))
                }
            )
        }
    }
};
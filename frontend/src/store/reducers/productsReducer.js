import {FETCH_ONE_ITEM_SUCCESS, FETCH_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actions/productsActions";

const initialState = {
    products: [],
    oneProduct: null,
    oneItem: null
};

const usersReducer = (state = initialState, action) => {
         switch (action.type) {
             case FETCH_PRODUCTS_SUCCESS:
                 return {...state, products: action.products};
             case FETCH_PRODUCT_SUCCESS:
                 return {...state, oneProduct: action.product};
             case FETCH_ONE_ITEM_SUCCESS:
                 return {...state, oneItem: action.product};
             default:
                 return state
         }
};

export default usersReducer
import {createStore, applyMiddleware, compose, combineReducers} from 'redux'
import thunkMiddleware from 'redux-thunk'

import {createBrowserHistory} from "history";

import {connectRouter, routerMiddleware} from "connected-react-router";

import usersReducer from '../store/reducers/usersReducer';
import categoriesReducer from '../store/reducers/categoriesReducer'
import productsReducer from '../store/reducers/productsReducer'

import {saveToLocalStorage, loadFromLocalStorage} from './localStorage';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    users: usersReducer,
    products: productsReducer,
    categories: categoriesReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(thunkMiddleware, routerMiddleware(history)));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    })
});

export default store
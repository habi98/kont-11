import React, {Component} from 'react';
import {
    Card,
    CardBody,
    CardColumns,
    CardImg,
    CardText,
    CardTitle,
} from "reactstrap";

import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";



class Products extends Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        return (
            <CardColumns>
                {this.props.products.map(product => (
                    <Link key={product._id} to={"/product/" + product._id}>
                        <Card >
                            <CardImg top width="100%" src={"http://localhost:8000/uploads/" + product.image} alt="product" />
                            <CardBody>
                                <CardTitle>{product.title}</CardTitle>
                                <CardText>Total {product.price}</CardText>
                            </CardBody>
                        </Card>
                    </Link>
                ))}
            </CardColumns>
        );
    }
}

const mapStateToProps = state => ({
    products: state.products.products,
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts()),

});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";
import {fetchProduct} from "../../store/actions/productsActions";

class OneProduct extends Component {

    componentDidMount() {
       this.props.fetchProduct(this.props.match.params.id)
    }

    render() {
        return (
            <Fragment>
                {this.props.product && (
                    <Link to={"product" + this.props.product._id}>
                        <Card>
                            <CardImg top width="100%" src={"http://localhost:8000/uploads/" + this.props.product.image} alt="product" />
                            <CardBody>
                                <CardTitle>{this.props.product.title}</CardTitle>
                                <CardText>Total {this.props.product.price}</CardText>
                            </CardBody>
                        </Card>
                    </Link>
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    product: state.products.oneProduct
});

const mapDispatchToProps = dispatch => ({
    fetchProduct: id => dispatch(fetchProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneProduct);
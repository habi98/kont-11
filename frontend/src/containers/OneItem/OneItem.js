import React, {Component, Fragment} from 'react';
import {fetchOneProduct} from "../../store/actions/productsActions";
import {connect} from "react-redux";
import {Button, Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";

class OneItem extends Component {
    componentDidMount() {
        this.props.fetchOneProduct(this.props.match.params.id)
    }

    render() {
        return (
           <Fragment>
               {this.props.oneItem && (
                   <Card>
                           <CardImg top style={{width: '300px',}} src={"http://localhost:8000/uploads/" + this.props.oneItem.image} alt="Card image cap" />

                       <CardBody >
                           <CardTitle>{this.props.oneItem.title}</CardTitle>
                           <CardText>{this.props.oneItem.description}</CardText>
                           <CardText>{this.props.oneItem.user.displayName} : +{this.props.oneItem.user.phone}</CardText>
                           <Button>Button</Button>
                       </CardBody>
                   </Card>
               )}
           </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    oneItem: state.products.oneItem
});

const mapDispatchToProps = dispatch => ({
    fetchOneProduct: id => dispatch(fetchOneProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneItem);
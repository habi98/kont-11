import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesAction";
import {createProduct} from "../../store/actions/productsActions";

class AddNewItem extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        category: '',
        price: ''
    };

    componentDidMount() {
        this.props.fetchCategories()
    }

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.createProduct(formData)
    };

    render() {
        return (
            <Fragment>
                <h2>Add new item</h2>

                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="category">Category</Label>
                        <Col sm={10}>
                            <Input
                                type="select" required
                                name="category" id="category"
                                value={this.state.category}
                                onChange={this.inputChangeHandler}
                            >
                                {this.props.categories.map(category => (
                                    <option key={category._id} value={category._id}>{category.title}</option>
                                ))}
                            </Input>
                        </Col>
                    </FormGroup>
                    <FormElement
                        title="Title"
                        propertyName="title"
                        type="text"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        title="Description"
                        propertyName="description"
                        type="textarea"
                        value={this.state.description}
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        title="Price"
                        propertyName="price"
                        type="number"
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        title="Image"
                        propertyName="image"
                        type="file"
                        onChange={this.fileChangeHandler}
                    />

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="success">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
});

const mapDispatchToProps = disptch => ({
    fetchCategories: () => disptch(fetchCategories()),
    createProduct: productData => disptch(createProduct(productData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewItem);
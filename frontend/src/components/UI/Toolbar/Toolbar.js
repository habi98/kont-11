import React from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler} from "reactstrap";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">Shop</NavbarBrand>
            <NavbarToggler />
            <Collapse navbar>
                <Nav className="ml-auto" navbar>
                    {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
                </Nav>
            </Collapse>
        </Navbar>
    );
};

export default Toolbar;
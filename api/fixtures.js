const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }


    const users = await User.create(
        {
            username: "alex_kim",
            password: "123",
            displayName: "Alex Kim",
            phone: +996555555555
        },
        {
            username: "Ben_Ascrin",
            password: "123",
            displayName: "Ben Ascrin",
            phone: +996553332244
        },
        {
            username: "john_doe",
            password: "123",
            displayName: "John Doe",
            phone: +996553442423
        }

    );


    const categories = await Category.create(
        {title: 'Computers'},
        {title: 'CPUs', description: 'Central Processing Units'},
        {title: 'HDDs', description: 'Hard Disk Drives'}
    );

    await Product.create(
        {
            title: 'Neo Game System Unit',
            price: 79000,
            description: '\n' +
                'HDD hard drive capacity, GB: ' +
                '2000 | Operating system: DOS | Processor Series: ' +
                'Intel Core i7-8th | RAM size and configuration: 16 | Model ' +
                'discrete graphics card: NVIDIA GeForce GTX1050 Ti | Size of ' +
                'a discrete video card, GB: 4',
            image: 'Neo.jpeg',
            category: categories[0]._id,
            user: users[0]._id
        },
        {
            title: 'Intel Core i7',
            price: 500,
            description: 'Very cool CPU',
            image: 'cpu.jpg',
            category: categories[1]._id,
            user: users[1]._id
        },
        {
            title: 'Toshiba 500GB',
            price: 60,
            description: 'Just a simple HDD',
            image: 'hdd.jpg',
            category: categories[2]._id,
            user: users[2]._id
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});

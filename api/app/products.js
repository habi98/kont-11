const express = require('express');
const auth = require('../middleware/auth');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const Product = require('../models/Product');

const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


router.get('/',  (req, res) => {

    let query;
    if (req.query.category) {
        query = {category: req.query.category}
    }

    Product.find(query).populate('user')
        .then(products => res.send(products))
        .catch(() => res.sendStatus(500))
});

router.get('/:id',  (req, res) => {

    Product.findById(req.params.id).populate(['user', 'category'])
        .then(post => res.send(post))
        .catch(() => res.sendStatus(500))
});


router.post('/', upload.single('image'), auth, (req, res) => {

    console.log(req.body);

    if (req.file) {
        req.body.image = req.file.filename
    }

    const product = new Product(req.body);

    product.user = req.user._id;

    product.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))

});


module.exports = router;



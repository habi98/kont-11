const express = require('express');

const User = require('../models/User');
const router = express.Router();



router.post('/', (req, res) => {

    console.log(req.body)

    const user = new User(req.body);

    user.generateToken();

    user.save()
        .then(user => res.send(user))
        .catch(error => res.status(400).send(error))
});


router.post('/sessions', async (req, res) => {

    const user = await User.findOne({username: req.body.username});


    if (!user) {
        return res.status(400).send({error: 'Username not found'})
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password id wrong'})
    }

    user.generateToken();

    await user.save();

    res.send(user)

});



router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logget logout'};

    if (!token) {
        return res.send(success)
    }

    const user  = await  User.findOne({token});

    if (!user) {
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)
});


module.exports = router;
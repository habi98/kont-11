const express =require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');


const users = require('./app/users');
const products = require('./app/products');
const categories = require('./app/categories');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/products', products);
    app.use('/categories', categories);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    })
});

